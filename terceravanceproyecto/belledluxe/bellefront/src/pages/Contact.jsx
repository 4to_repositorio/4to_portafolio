import NavContact from '../components/contact/NavContact';
import Textocontact from '../components/contact/Textocontact';
import { Prueba2 } from '../components/contact/Prueba2';
import BoxAbout from '../components/about/BoxAbout';

import '../css/contact/Contac.css';
import '../css/contact/formulario.css';

import FooterHome from "../components/FooterHome.jsx"

const containerStyle = {
  backgroundColor: "#11131F",
  border: "1px solid white",

};

export default function Contact() {
  return (


    <div style={containerStyle}>
      <NavContact />
      <Textocontact />

      <div className="contact-container">
        <div className="half-page">
          <BoxAbout
            title="Estimado Usuario:"
            description="En BelleD'Luxe, nos dedicamos a tu bienestar. Eres nuestra máxima prioridad en la tienda. Por lo tanto, te solicitamos de manera cortés que, en caso de cualquier inconveniente, consulta o deseo de dejarnos un mensaje, no dudes en completar nuestro formulario. Nos comprometemos a brindarte atención lo más pronta posible."
          />
        </div>
      <div className="half-page">
          <Prueba2 />
      </div>
    </div>
      <FooterHome />

    </div>


  );
}