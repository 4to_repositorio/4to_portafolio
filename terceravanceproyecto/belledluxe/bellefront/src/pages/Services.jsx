import NavServices from '../components/blog/NavBlog';
import TextS from '../components/guest/TextS';
import TabsS from '../components/guest/TabsS';

import FooterHome from '../components/FooterHome';

const containerStyle = {
    backgroundColor: "#11131F",
    border: "1px solid white",
    fontFamily: "Times New Roman, serif",
  };
  
export default function Blog() {
    return (
      <div style={containerStyle}>
        <NavServices/>
        <TextS/>
        <TabsS/>

        <br/>
        <br />
        <br />
        <br />
        <FooterHome/>

      </div>
    );
  }
  