import React from "react";
import ReItemPedido from "../../../components/ApiCruds/itempedido/ReItemPedido";
import AdminSlidebar from "../../../components/AdminSlidebar";

export default function ItemPedido(){
    return(
        <div className="min-h-screen flex">
        <AdminSlidebar />
        <div className="flex-1 p-6">
        <ReItemPedido/>
        </div>
      </div>
    );
}