import React, { useEffect, useState } from "react";
import { useForm } from 'react-hook-form';
import { getItemPedido, UpdateItemPedido, DeleteItemPedido } from "../../../api/itempedido_api";
import { getAllPedidos } from "../../../api/Pedido_api";
import { getAllProducts } from "../../../api/Producto_api";
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-hot-toast';
import NavApi from "../../../components/ApiCruds/NavApi";
import FooterApi from "../../../components/ApiCruds/FooterApi";

export default function ItemPedidoForm() {
    const [pedidos, setPedidos] = useState([]);
    const [productos, setProductos] = useState([]);
    const { register, handleSubmit, formState: { errors }, setValue } = useForm();
    const navigate = useNavigate();
    const params = useParams();
    const handleClick = () => {
        navigate('../ReItemPedido/');
    };

    useEffect(() => {
        async function fetchData() {
            const pedidosResponse = await getAllPedidos();
            setPedidos(pedidosResponse.data);

            const productosResponse = await getAllProducts();
            setProductos(productosResponse.data);
 
            if (params.id) {
                const { data } = await getItemPedido(params.id);
                setValue('pedido', data.pedido);
                setValue('product', data.product);
                setValue('cantidad', data.cantidad);
                setValue('codigo_producto', data.codigo_producto);
            }
        }
        fetchData();
    }, [params.id, setValue]);

    const onSubmit = handleSubmit(async data => {
        if (params.id) {
            await UpdateItemPedido(params.id, data);
            toast.success('SubPedido actualizado con éxito');
        } else {
            toast.error('SubPedido no encontrado, inténtelo de nuevo');
        }
        navigate('../ReItemPedido');
    });

    return (
        <div>
            <NavApi/>

        <div className="container mx-auto mt-10 text-white">
            <form onSubmit={onSubmit} className="max-w-md mx-auto">
                <label htmlFor="pedido" className="block mb-2">Pedido:</label>
                <select id="pedido" {...register("pedido", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white">
                    {pedidos.map(pedido => (
                        <option key={pedido.id} value={pedido.id}>{pedido.id}</option>
                    ))}
                </select>
                {errors.pedido && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="product" className="block mb-2">Producto:</label>
                <select id="product" {...register("product", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white">
                    {productos.map(producto => (
                        <option key={producto.id} value={producto.id}>{producto.name}</option>
                    ))}
                </select>
                {errors.product && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="cantidad" className="block mb-2">Cantidad:</label>
                <input type="number" id="cantidad" {...register("cantidad", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Cantidad" />
                {errors.cantidad && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="codigo_producto" className="block mb-2">Código del producto:</label>
                <input type="text" id="codigo_producto" {...register("codigo_producto", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Código del producto" />
                {errors.codigo_producto && <span className="text-red-500">Este campo es requerido</span>}



 
                <button type="submit" className="block w-full px-4 py-2 bg-blue-500 rounded hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Guardar</button>
        
                {params.id &&
                <button onClick={async () => {
                    const accepted = window.confirm("¿Estás seguro de que deseas eliminar este SubPedido definitivamente?");
                    if (accepted) {
                        await DeleteItemPedido(params.id);
                        navigate('../ReItemPedido');
                        toast.success('Subpedidido eliminado');
                    }
                }} className="block w-full px-4 py-2 mt-4 bg-red-500 rounded hover:bg-red-600 focus:outline-none focus:bg-red-600">Eliminar</button>}

<br />
                <hr />
                <br />
                <button onClick={handleClick} className="block w-full px-4 py-2 bg-gray-500 rounded hover:bg-gray-600 focus:outline-none focus:bg-gray-600">Cancelar</button>

           </form>




        </div>
        <br />

            <FooterApi />
        </div>
    );
}
