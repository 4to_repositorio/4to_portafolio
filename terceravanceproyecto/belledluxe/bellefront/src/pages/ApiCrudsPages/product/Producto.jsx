import React from "react";
import ReProducto from "../../../components/ApiCruds/product/ReProducto";
import AdminSlidebar from "../../../components/AdminSlidebar";

export default function ProductoCrud(){
    return(
        <div className="min-h-screen flex">
        <AdminSlidebar />
        <div className="flex-1 p-6">
        <ReProducto/>
        </div>
      </div>
    ); 
}