import React, { useEffect, useState } from "react";
import { useForm } from 'react-hook-form';
import { getProduct, createProduct, UpdateProduct, DeleteProduct, uploadProductImage } from "../../../api/Producto_api";
import { getAllUsers } from "../../../api/Users";
import { getAllCampaigns } from "../../../api/Campaign_api";
import { getAllCategories } from "../../../api/Category_api";
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-hot-toast';
import NavApi from "../../../components/ApiCruds/NavApi";
import FooterApi from "../../../components/ApiCruds/FooterApi";

export default function ProductForm() {
    const [users, setUsers] = useState([]);
    const [campaigns, setCampaigns] = useState([]);
    const [categories, setCategories] = useState([]);
    const [data, setData] = useState({});
    const { register, handleSubmit, formState: { errors }, setValue } = useForm();
    const navigate = useNavigate();
    const params = useParams();
    const handleClick = () => {
        navigate('../ReProducto/');
    };

    useEffect(() => {
        async function fetchData() {
            const usersResponse = await getAllUsers();
            setUsers(usersResponse.data);

            const campaignsResponse = await getAllCampaigns();
            setCampaigns(campaignsResponse.data);

            const categoriesResponse = await getAllCategories();
            setCategories(categoriesResponse.data);


            if (params.id) {
                const { data } = await getProduct(params.id);
                setValue('lasteditor', data.lasteditor);
                setValue('name', data.name);
                setValue('campaign', data.campaign);
                setValue('gender', data.gender);
                setValue('category', data.category);
                setValue('description', data.description);
                setValue('size', data.size);
                setValue('price', data.price);
                setValue('stock', data.stock);
                setValue('slug', data.slug);
                setValue('status', data.status);

                if (data.imagen1 && data.imagen1.length > 0) {
                    setValue('imagen1', data.imagen1_url);
                }                
                if (data.imagenqr && data.imagenqr.length > 0) {
                    setValue('imagenqr', data.imagenqr_url);
                }
            }
        }
        fetchData();
    }, [params.id, setValue]);

    const onSubmit = handleSubmit(async (data) => {
        if (params.id) {
            await UpdateProduct(params.id, data);
            const formData = new FormData();

            if (data.imagen1 && data.imagen1.length > 0) {
                formData.append('imagen1', data.imagen1[0]); 
            } else {
                formData.append('imagen1', null); // Establece el valor de imagenqr como null si no se selecciona ninguna imagen
            }


            if (data.imagenqr && data.imagenqr.length > 0) {
                formData.append('imagenqr', data.imagenqr[0]); 
            } else {
                formData.append('imagenqr', null); // Establece el valor de imagenqr como null si no se selecciona ninguna imagen
            }

            toast.success('Producto actualizado con éxito');


        } else {
            await createProduct(data);
            const formData = new FormData();
            formData.append('imagen1', data.imagen1 ? data.imagen1[0] : null); 
            formData.append('imagenqr', data.imagenqr ? data.imagenqr[0] : null); // Establece el valor de imagenqr como null si no se selecciona ninguna imagen
            toast.success('Producto agregado con éxito');
        }
        
        navigate('../ReProducto');
    });
    
    
        

    const handleImageChange = (e, fieldName) => {
        const file = e.target.files[0];
        let newData = { ...data };
        if (file) {
            newData[fieldName] = file;
        } else {
            // If no file is selected, keep the existing value
            newData[fieldName] = data[fieldName];
        }
          // newData[fieldName] = file || null;
        setData(newData);
        setValue(fieldName, file || null); 
    };



    return (
        <div>
            <NavApi/>
        <div className="container mx-auto mt-10 text-white">
            <form encType="multipart/form-data" onSubmit={onSubmit} className="max-w-md mx-auto">
                <br />

                <label htmlFor="lasteditor" className="block mb-2">Último Editor:</label>
                <select id="lasteditor" {...register("lasteditor", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white">
                    {users.filter(user => user.is_superuser).map(user => (
                        <option key={user.id} value={user.id}>{user.username}</option>
                    ))}
                </select>
                {errors.lasteditor && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="name" className="block mb-2">Nombre:</label>
                <input type="text" id="name" {...register("name", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Nombre" maxLength={30}/>
                {errors.name && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="campaign" className="block mb-2">Campaña:</label>
                <select id="campaign" {...register("campaign", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white">
                    {campaigns.map(campaign => (
                        <option key={campaign.id} value={campaign.id}>{campaign.name}</option>
                    ))}
                </select>
                {errors.campaign && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="gender" className="block mb-2">Género:</label>
                <select id="gender" {...register("gender", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white">
                    <option value="Hombre">Hombre</option>
                    <option value="Mujer">Mujer</option>
                </select>
                {errors.gender && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="category" className="block mb-2">Categoría:</label>
                <select id="category" {...register("category", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white">
                    {categories.map(category => (
                        <option key={category.id} value={category.id}>{category.name}</option>
                    ))}
                </select>
                {errors.category && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="description" className="block mb-2">Descripción:</label>
                <textarea id="description" {...register("description", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Descripción" maxLength={210}/>
                {errors.description && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="size" className="block mb-2">Talla:</label>
                <input type="text" id="size" {...register("size", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Talla" maxLength={3}/>
                {errors.size && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="price" className="block mb-2">Precio Unitario:</label>
                    <input type="number" id="price"  className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Precio" maxLength={8}
                        {...register("price", {required: true, validate: {
                            isNumber: (value) => {
                                const num = parseFloat(value);
                                return!isNaN(num) && isFinite(num);
                            },
                            isPositive: (value) => parseFloat(value) > 0,
                            },
                        })}
                    />
                        {errors.price && errors.price.type === "required" && (
                        <span className="text-red-500"> El precio es requerido.</span>
                        )}
                        {errors.price && errors.price.type === "isNumber" && (
                        <span className="text-red-500">El precio debe ser un número válido.</span>
                        )}
                        {errors.price && errors.price.type === "isPositive" && (
                        <span className="text-red-500">El precio debe ser un número positivo.</span>
                        )}

                <label htmlFor="stock" className="block mb-2">Cantidad:</label>
                <input type="number" id="stock" className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Cantidad" maxLength={4}
                    {...register("stock", {required: true, validate: {
                        isNumber: (value) => {
                            const num = parseFloat(value);
                            return!isNaN(num) && isFinite(num);
                        },
                        isPositive: (value) => parseFloat(value) >= 0,
                        },
                    })}
                />
                {errors.stock && errors.stock.type === "isPositive" && (
                        <span className="text-red-500">Debe ser un número positivo.</span>
                        )}

                <label htmlFor="slug" className="block mb-2">Slug:</label>
                <input type="text" id="slug" {...register("slug", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Slug" maxLength={10}/>
                {errors.slug && <span className="text-red-500">Este campo es requerido</span>}
                

                <label htmlFor="status" className="block mb-2">Status:</label>
                <input type="checkbox" id="status" {...register("status")} />

                <br />
                    
                    Imagen de referencia : 
                    <input type="file" name="imagen1" accept="/jpeg,/png" {...register( "imagen1")} 
                    onChange={(e) => {
                            handleImageChange(e);
                        }} 
                    />
                    {errors.imagen1 && <span className="text-red-500">La imagen es requerida.</span>}
                
                <br /> 
                <br />

                Imagen QR : 
                    <input type="file" name="imagenqr" accept="/jpeg,/png" {...register( "imagenqr")} 
                    onChange={(e) => {
                            handleImageChange(e);
                        }} 
                    />
                    {errors.imagenqr && <span className="text-red-500">Ingrese imagen QR . </span>}
                
                <br /> 
                <br />


                <button type="submit" className="block w-full px-4 py-2 bg-blue-500 rounded hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Guardar</button>


                {params.id &&
                <button onClick={async () => {
                    const accepted = window.confirm("¿Estás seguro de que deseas eliminar este producto definitivamente?");
                    if (accepted) {
                        await DeleteProduct(params.id);
                        navigate('../ReProducto');
                    }
                }} className="block w-full px-4 py-2 mt-4 bg-red-500 rounded hover:bg-red-600 focus:outline-none focus:bg-red-600">Eliminar</button>}

                <br />
                <hr />
                <br />
                <button onClick={handleClick} className="block w-full px-4 py-2 bg-gray-500 rounded hover:bg-gray-600 focus:outline-none focus:bg-gray-600">Cancelar</button>


            </form>




            
        </div>

        <br />

            <FooterApi />
        </div>
    );
}