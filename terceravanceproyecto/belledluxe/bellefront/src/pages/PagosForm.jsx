import React, { useState } from 'react';
import NavApi from "../../../components/ApiCruds/NavApi";
import FooterApi from "../../../components/ApiCruds/FooterApi";


function PagosForm() {
    const [formData, setFormData] = useState({
        numeroTarjeta: '',
        fecha: '',
        cvv: ''
    });
    const [errors, setErrors] = useState({});

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.id]: e.target.value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        const errors = {};

        if (formData.numeroTarjeta.length !== 16 || !/^\d+$/.test(formData.numeroTarjeta)) {
            errors.numeroTarjeta = 'Por favor ingresa un número de tarjeta válido de 16 dígitos.';
        }

        if (!/^(0[1-9]|1[0-2])\/([0-9]{2})$/.test(formData.fecha)) {
            errors.fecha = 'Por favor ingresa una fecha de expiración válida en formato MM/AA.';
        }

        if (formData.cvv.length !== 3 || !/^\d+$/.test(formData.cvv)) {
            errors.cvv = 'Por favor ingresa un CVV válido de 3 dígitos.';
        }

        if (Object.keys(errors).length === 0) {

            // <a href="https://buy.stripe.com/7sI7vMekpbnbbjqeUV">
            //     Pagar por Stripe
            // </a>

            console.log('Datos válidos, enviar a Stripe:', formData);
        } else {
            setErrors(errors);
        }
    };

    return (
        <div>
            <NavApi />
        <form className="max-w-md mx-auto" onSubmit={handleSubmit}>
            <br />
            <p>**Tener en cuenta que el proceso de pagos por stripe puede tomar días</p>

            <label htmlFor="numeroTarjeta" className="block mb-2">Número de tarjeta :</label>
            <input
                type="text"
                id="numeroTarjeta"
                className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"
                placeholder="16 dígitos"
                value={formData.numeroTarjeta}
                onChange={handleChange}
            />
            {errors.numeroTarjeta && <div className="text-red-500">{errors.numeroTarjeta}</div>}

            <label htmlFor="fecha" className="block mb-2">Fecha de expiración :</label>
            <input
                type="text"
                id="fecha"
                className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"
                placeholder="MM/AA"
                value={formData.fecha}
                onChange={handleChange}
            />
            {errors.fecha && <div className="text-red-500">{errors.fecha}</div>}

            <label htmlFor="cvv" className="block mb-2">CVV :</label>
            <input
                type="text"
                id="cvv"
                className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"
                placeholder="3 dígitos"
                value={formData.cvv}
                onChange={handleChange}
            />
            {errors.cvv && <div className="text-red-500">{errors.cvv}</div>}

            <button type="submit" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                Pagar por Stripe
            </button>
        </form>

        <FooterApi />
        </div>

    );
}

export default PagosForm;
