import NavAbout from "../components/about/NavAbout";
import TextAbout from "../components/about/TextAbout";
import HeaderAbout from "../components/about/HeaderAbout";
import InfoAbout from "../components/about/InfoAbout";
import BoxAbout from "../components/about/BoxAbout";
import FooterHome from "../components/FooterHome";

import logobdl from "../assets/images/logobdl.png";

import "../css/about/aboutInfo.css";
import "../css/about/aboutBox.css";

const containerStyle = {
  backgroundColor: "#11131F",
  border: "1px solid white",
  fontFamily: "Times New Roman, Baskerville, serif",
};

export default function About() {
  return (
    <div style={containerStyle}>
      <NavAbout />
      <TextAbout />

      <div className="contenedor">
        <div className="columna">
          <div>{<InfoAbout />}</div>
        </div>
        <div className="columna">
          <img
            src={logobdl}
            alt="Descripción de la imagen"
            className="imagen"
          />
        </div>
      </div>

      <div className="flex-container">
        <BoxAbout
          title="Filosofía"
          description="“Los lujos normalmente se entienden como cosas que se adquieren con un alto valor, pero en BELLE D’ LUXE el lujo es sentir comodidad y confianza en cada prenda, generando belleza incondicional única en cada persona”."
        />
        <BoxAbout
          title="Misión"
          description="“Innovar en la creación de la moda, sin daños al entorno natural, creando una sensación de comodidad, seguridad y confianza a nuestros consumidores con cada prenda, obteniendo una belleza de lujo única”."
        />
        <BoxAbout
          title="Visión"
          description="“Ser el desafío a la diversidad, anticipando la igualdad, diseñando y ofreciendo productos innovadores, novedosos y sofisticados, de lujo, dándole un toque de glamour, y promover la confianza y la personalidad“"
        />
      </div>

      <HeaderAbout></HeaderAbout>
      <FooterHome></FooterHome>
    </div>
  );
}
