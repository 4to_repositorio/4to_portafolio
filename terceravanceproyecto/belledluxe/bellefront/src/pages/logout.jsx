import { logout } from "../utils/auth"
import { useEffect } from "react"
import { useNavigate } from 'react-router-dom'

export default function Logout() {
    const navigate = useNavigate();
  
    useEffect(() => {
      logout();
      navigate('/');
    }, [navigate]);
  
    return null;
  };