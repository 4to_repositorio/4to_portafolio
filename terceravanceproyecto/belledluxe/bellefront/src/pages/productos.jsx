import React, { useState, useEffect } from 'react';
import axios from '../utils/axios';
import { useParams, useNavigate } from 'react-router-dom';
import NavAbout from '../components/about/NavAbout';
import { useAuthStore } from '../store/auth';
import { agCarrito } from '../utils/carrito';
import '../css/tienda/product.css';
import FooterHome from "../components/FooterHome";

function Producto() {
    const nav = useNavigate();
    const estalog = useAuthStore((state) => state.isLoggedIn)();
    const { num } = useParams();
    const [product, setProduct] = useState({});
    const [selectedQty, setSelectedQty] = useState(1);
    
    useEffect(() => {
        axios.get(`producto/${num}`)
            .then(response => {
                setProduct(response.data);
            })
            .catch(error => {
                console.error('Error fetching product:', error);
            });
    }, [num]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const n2 = parseInt(selectedQty);
        const n1 = parseInt(num);
        await agCarrito(n1, n2);
    };

    if (!product.id) {
        return <div>Cargando...</div>;
    }

    return (
        <div>
        <NavAbout />
        <div className='text-white border'>
            
            <div className="p-4">
                <h1 className='text-center text-5xl font-bold mb-8'>{product.name}</h1>
                <div className="flex flex-col md:flex-row">
                    <div className="md:w-1/2 p-4">
                        <img src={product.imagen1} alt={product.name} className="mx-auto mb-4" />
                        <img src={product.imagenqr} alt={product.name} className="mx-auto" />
                    </div>
                    <div className="md:w-1/2 p-4">
                        <p className='text-md'><strong>Descripción:</strong> {product.description}</p>
                        <p className='text-md'><strong>Talla:</strong> {product.size}</p>
                        <p className='text-md'><strong>Precio (Unidad):</strong> $ {product.price}</p>
                        <p className='text-md'><strong>Stock:</strong> {product.stock}</p>
                        <form onSubmit={handleSubmit} className='mt-8'>
                            <label htmlFor="quantity" className="block text-md font-semibold mb-2">Cantidad:</label>
                            <select style={{color:'black'}}
                                id="quantity"
                                name='quantity'
                                value={selectedQty}
                                onChange={(e) => setSelectedQty(e.target.value)}
                                className="form-select block w-full py-2 px-3 border rounded-md  sm:text-lg"
                            >
                                {[...Array(product.stock).keys()].map((_, index) => (
                                    <option key={index + 1} value={index + 1}>{index + 1}</option>
                                ))}
                            </select>
                            <div className="mt-8">
                                <button className='add border  text-white' style={{fontSize: "18px"}} type="submit">
                                    Añadir al carrito
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <FooterHome />
        </div>
        </div>
    );
}

export default Producto;
