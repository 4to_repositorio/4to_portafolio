import axios from 'axios'

const PedidosApi = axios.create({
    baseURL:'http://localhost:8000/api/cruds/pedido/',
    
})

export const getAllPedidos = () => PedidosApi.get('/');
export const getPedido = (id) => PedidosApi.get('/' + id + '/');
export const DeletePedido = (id) => PedidosApi.delete('/' + id); 
export const UpdatePedido= (id, Pedido) => PedidosApi.put(`/${id}/`, Pedido );
