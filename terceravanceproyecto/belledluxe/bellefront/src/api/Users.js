import axios from 'axios'
import Cookies from 'js-cookie';

const UsersApi = axios.create({
    baseURL:'http://localhost:8000/api/ver_usuarios/',

})

export const getAllUsers  = () => UsersApi.get('/');
