import axios from 'axios'


const ItempedidoApi = axios.create({
    baseURL:'http://localhost:8000/api/cruds/itemPedido/',

})

export const getAllItemPedidos = () => ItempedidoApi.get('/');
export const getItemPedido = (id) => ItempedidoApi.get('/' + id + '/');
export const DeleteItemPedido = (id) => ItempedidoApi.delete('/' + id); 
export const UpdateItemPedido= (id, ItemPedido) => ItempedidoApi.put(`/${id}/`, ItemPedido );
