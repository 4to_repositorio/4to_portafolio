import { useAuthStore } from '../store/auth';
import axios from './axios';
import jwt_decode from 'jwt-decode';
import Cookies from 'js-cookie';

export const login = async (username, password) => {
    try {
        const { data, status } = await axios.post('token/', {
            username,
            password,
        });
        if (status === 200) {
            setAuthUser(data.access, data.refresh);
            
            
            const newAdminStatus = await checkAdminStatus();
            Cookies.set('checkAdminStatus', newAdminStatus, {
                expires: 1, 
                secure: true, })
            useAuthStore.getState().setadmin(Cookies.get(newAdminStatus)==='true');
        
            
        }
        return { data, error: null };
    } catch (error) {
        return {
            data: null,
            error: error.response.data?.detail || 'Algo malo paso',
        };
    }
};

export const register = async (username,  firstName, lastName, lastName2, phone, password, password2) => {
    try {
        const { data } = await axios.post('register/', {
            username,
            firstName,
            lastName,
            lastName2,
            phone,
            password,
            password2,
        });
        await login(username, password);
        return { data, error: null };
    } catch (error) {
        return {
            data: null,
            error: error.response.data || 'Algo malo paso',
        };
    }
};

export const logout = () => {
    Cookies.remove('access_token');
    Cookies.remove('refresh_token');
    Cookies.remove('checkAdminStatus');
    useAuthStore.getState().setUser(null);
    useAuthStore.getState().setadmin(false);
    
};

export const setUser = async () => {
    const accessToken = Cookies.get('access_token');
    const refreshToken = Cookies.get('refresh_token');
    
    useAuthStore.getState().setadmin(Cookies.get('checkAdminStatus')==='true');
    
    if (!accessToken || !refreshToken) {
        return;
    }
    if (isAccessTokenExpired(accessToken)) {
        const response = await getRefreshToken(refreshToken);
        setAuthUser(response.access, response.refresh);
    } else {
        setAuthUser(accessToken, refreshToken);
    }

};

export const setAuthUser = (access_token, refresh_token) => {
    Cookies.set('access_token', access_token, {
        expires: 1,
        secure: true,
    });

    Cookies.set('refresh_token', refresh_token, {
        expires: 7,
        secure: true,
    });
    const adminStatusCookie = Cookies.get('checkAdminStatus');
    
    if (adminStatusCookie === undefined || adminStatusCookie === null) {
        checkAdminStatus().then(newAdminStatus => {
            Cookies.set('checkAdminStatus', newAdminStatus, {
                expires: 1, 
                secure: true, 
            });
            useAuthStore.getState().setadmin(newAdminStatus);
        }).catch(error => {
            console.error('Error al obtener el estado de administrador:', error);
        });
    }

    const user = jwt_decode(access_token) ?? null;
    if (user) {
        useAuthStore.getState().setUser(user);
    }
    useAuthStore.getState().setLoading(false);
};

export const getRefreshToken = async () => {
    const refresh_token = Cookies.get('refresh_token');
    const response = await axios.post('token/refresh/', {
        refresh: refresh_token,
    });
    return response.data;
};
export const isAccessTokenExpired = (accessToken) => {
    try {
        const decodedToken = jwt_decode(accessToken);
        return decodedToken.exp < Date.now() / 1000;
    } catch (err) {
        return true;
    }
};

export const checkAdminStatus = async () => {
    try {
        const accessToken = Cookies.get('access_token');
        const response = await axios.post('/check-admin-status/', null, {
        headers: { Authorization: `Bearer ${accessToken}` } // este es donde se pone el 
      });
  
      return response.data.isAdmin;
    } catch (error) {
      console.error('Error checking admin status:', error);
      return false;
    }
  };

export const direccionPost = async (streat,cp,number_home) => {
    const accessToken = Cookies.get('access_token');
    try {
        const { data } = await axios.post('register2/', {
            streat,
            cp,
            number_home
        },{
            headers: {
              'Authorization': `Bearer ${accessToken}`
            }
          });
        
        return { data, error: null };
    } catch (error) {
        return {
            data: null,
            error: error.response.data || 'Algo malo paso',
        };
    }
};




