import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import { getAllProducts } from "../../../api/Producto_api";
import VistProduct from "./VistProducto";

export default function ReProducto() {
    const [Product, setProducts] = useState([]);

    useEffect(() => {
        async function loadProducts(){
            const resp = await getAllProducts();
            setProducts(resp.data);
        }
        loadProducts();
    }, []);

    return (
        <div className="bg-gray-900 text-white min-h-screen p-8">
            <div className="flex justify-between items-center mb-6">
                <h1 className="text-3xl font-bold">Productos</h1>
                <Link to="../ProductForm" className="bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Crear nuevo producto</Link>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
                {Product.map(Product => (
                    <VistProduct key={Product.id} Product={Product} />
                ))}
            </div>
        </div>
    );
}
