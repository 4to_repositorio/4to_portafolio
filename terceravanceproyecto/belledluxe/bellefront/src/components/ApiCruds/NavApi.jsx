import React, { useState, useEffect } from "react";
import { Menu } from "@headlessui/react";
import {ChevronLeftIcon } from "@heroicons/react/outline";
import logobdl from "../../assets/images/logobdl.png";
import { useNavigate } from 'react-router-dom';


export default function NavApi(){
    
        const navigate = useNavigate();
        const navBackgroundColor = "#11131F";
     
      return (
        <nav
          className="p-6 flex justify-between items-center"
          style={{
            backgroundColor: navBackgroundColor,
            borderBottom: "1px solid white",
          }}
        >

          <div className="flex items-center">
    
            <button onClick={() => navigate(-1)}>
            <ChevronLeftIcon className="h-6 w-6 text-white mr-4" />
            </button>

              <a href="/adm">
                <img
                  src={logobdl}
                  alt="logo"
                  style={{ width: "250px", height: "auto" }}
                />
              </a>

          </div>




        </nav>
      );
}






