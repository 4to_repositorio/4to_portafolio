import React from 'react';
import { useNavigate } from 'react-router-dom';

export default function VistPedido({ Pedido }) {
    const navigate = useNavigate();

    const handleClick = () => {
        navigate('../RePedido/' + Pedido.id);
    };

    return (
        <div
            style={{
                border: '1px solid #eee',
                borderRadius: '8px',
                padding: '20px',
                marginBottom: '20px',
                cursor: 'pointer',
                transition: 'all 0.3s ease',
                boxShadow: '0 2px 4px rgba(0, 0, 0, 0.1)',
                backgroundColor: '#fff',
                color: '#333',
            }}
            onClick={handleClick}
        >
            <h1 style={{ margin: '5px 0', fontSize: '22px', color: '#555' }}>ID: {Pedido.id}</h1>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Fecha: {Pedido.fecha}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Código: {Pedido.codigo_pedido}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Usuario: {Pedido.usuario}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Productos: {Pedido.productos}</p>
        </div>
    );
}
