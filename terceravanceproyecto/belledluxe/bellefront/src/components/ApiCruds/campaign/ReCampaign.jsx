import React, { useEffect, useState } from "react";
import { getAllCampaigns } from "../../../api/Campaign_api";
import VistCampaigns from "./VistCampaigns";
import { useNavigate } from 'react-router-dom';

export default function ReCampaign() {
    const [campaigns, setCampaigns] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        async function loadCampaigns(){
            const resp = await getAllCampaigns();
            setCampaigns(resp.data);
        }
        loadCampaigns();
    }, []);

    return (
        <div className="bg-gray-900 text-white min-h-screen p-8">
            <div className="flex justify-between items-center mb-6">
                <h1 className="text-3xl font-bold">Campañas</h1>
                <button onClick={() => navigate('../CampaignForm')} className="bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Crear nueva campaña</button>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
                {campaigns.map(campaign => (
                    <VistCampaigns key={campaign.id} campaign={campaign} />
                ))}
            </div>
        </div>
    );
}
