import React, { useState, useEffect } from "react";
import { Menu } from "@headlessui/react";
import { ShoppingCartIcon, UserCircleIcon, ChevronLeftIcon } from "@heroicons/react/outline";
import logobdl from "../../assets/images/logobdl.png";
import { useAuthStore } from "../../store/auth";
import { useNavigate } from 'react-router-dom';

const NavCart = () => {

  const navigate = useNavigate();

  const isAdmin = useAuthStore((state) => state.admin)
  
  const [isOpen, setIsOpen] = useState(false);
  const navBackgroundColor = "#11131F";

  const [superior, setsuperior] = useState('login');
  const [inferior, setinferior] = useState('register');
  const [link1, setlink1]= useState('login')
  const [link2, setlink2]= useState('singup')


  const isLoggedIn = useAuthStore((state) => state.isLoggedIn)  
  useEffect(()=>{
    if(isLoggedIn()){
      setsuperior('Perfil')
      setinferior('Salir')
      setlink1('/perfil')
      setlink2('/salir')
    }else{
      setsuperior('acceder')
      setinferior('registrar')
      setlink1('/login')
      setlink2('/register')
    }
  },[])
  return (
    <nav
      className="p-6 flex justify-between items-center"
      style={{
        backgroundColor: navBackgroundColor,
        borderBottom: "1px solid white",
      }}
    >
      <div className="flex items-center">
        <button onClick={() => navigate(-1)}>
        <ChevronLeftIcon className="h-6 w-6 text-white mr-4" />
        </button>

        <a href="/">
          <img
            src={logobdl}
            alt="logo"
            style={{ width: "250px", height: "auto" }}
          />
        </a>

        {/* Enlaces del menú en pantallas grandes */}
        <div className="hidden lg:flex">
          <a href="/" className="text-white mx-4">
            Tienda
          </a>
          <a href="/SERVICES" className="text-white mx-4">
            Servicios
          </a>
          <a href="/blog" className="text-white mx-4">
            Blog
          </a>
        </div>
      </div>

      <div className="flex justify items-center">
        {/* Menú desplegable en pantallas pequeñas */}
        <Menu as="div" className="lg:hidden">
          <Menu.Button>
            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-white" viewBox="0 0 20 20" fill="currentColor">
              <path fillRule="evenodd" d="M3 6a1 1 0 011-1h12a1 1 0 010 2H4a1 1 0 01-1-1zm0 5a1 1 0 011-1h12a1 1 0 010 2H4a1 1 0 01-1-1zm0 5a1 1 0 011-1h12a1 1 0 010 2H4a1 1 0 01-1-1z" clipRule="evenodd" />
            </svg>
          </Menu.Button>
          <Menu.Items className="absolute right-0 px-8 py-4 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
            <div>
              <Menu.Item>
                {({ active }) => (
                  <a
                    href="/tienda"
                    className={`${active ? "bg-gray-100" : ""} block py-2 `}
                  >
                    Tienda
                  </a>
                )}
              </Menu.Item>
              <Menu.Item>
                {({ active }) => (
                  <a
                    href="/servicios"
                    className={`${active ? "bg-gray-100" : ""} block`}
                  >
                    Servicios
                  </a>
                )}
              </Menu.Item>
              <Menu.Item>
                {({ active }) => (
                  <a
                    href="/blog"
                    className={`${active ? "bg-gray-100" : ""} block`}
                  >
                    Blog
                  </a>
                )}
              </Menu.Item>
              <div>
                <Menu.Item>
                  {({ active }) => (
                    <a
                      href={link1}
                      className={`${active ? "bg-gray-100" : ""} block py-2 `}
                    >
                      {superior}
                      <div>
                          {isAdmin ? (
                              <h1>Admin!</h1>
                              
                             
                          ) : (
                            <></>

                          )}
                      </div>
                    </a>
                    
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <a
                      href={link2}
                      className={`${active ? "bg-gray-100" : ""} block`}
                    >
                      {inferior}
                    </a>
                  )}
                </Menu.Item>
                {/* Otros elementos de menú */}
              </div>
              <Menu.Item>
                {({ active }) => (
                  <a
                    href="/carrito"
                    className={`${active ? "bg-gray-100" : ""} block`}
                  >
                    Carrito
                  </a>
                )}
              </Menu.Item>
            </div>
          </Menu.Items>
        </Menu>

        <Menu as="div" className="hidden lg:block">
          <Menu.Button>
            <UserCircleIcon className="h-6 w-6 text-white" />
          </Menu.Button>
          <Menu.Items className="absolute right-0 px-8 py-4 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
            <div>
              <Menu.Item>
                {({ active }) => (
                  <a
                    href="LogIn"
                    className={`${active ? "bg-gray-100" : ""} block py-2 `}
                  >
                    Log In
                  </a>
                )}
              </Menu.Item>
              <Menu.Item>
                {({ active }) => (
                  <a
                    href="Register"
                    className={`${active ? "bg-gray-100" : ""} block`}
                  >
                    Sign In
                  </a>
                )}
              </Menu.Item>
              {/* Otros elementos de menú */}
            </div>
          </Menu.Items>
        </Menu>
        
        {/* Icono de carrito */}
        <div className="ml-4 hidden lg:block">
          <ShoppingCartIcon className="h-6 w-6 text-white" />
        </div>
      </div>
    </nav>
  );
};

export default NavCart;


