import React, { useState } from 'react';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import {direccionPost} from '../../../utils/auth'
const FormDir = () => {

  const [streat, setstreat] = useState('');
  const [numberh, setnumber] = useState('');
  const [cp, setcp] = useState('');

  const navigate = useNavigate();
  
 

  const resetForm = () => {
    setstreat('');
    setnumber('');
    setcp('');
};
  const handleSubmit = async(e) => {
    direccionPost(streat,numberh,cp)
    resetForm();
    navigate('/');

  };

  return (
    <div className="flex justify-center items-center h-screen">
      <form onSubmit={handleSubmit} className="border shadow-md px-8 pt-6 pb-8 mb-4">
      <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="username">
          BELLE D' LUXE 
          <div style={{'color':'yellow'}}>*solo hace envios en Tijuana</div> 
          </label>
        </div>
        <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="username">
          Calle
          </label>
          <input
            className="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="streat"
            type="text"
            placeholder="Calle"
            name="streat"    
            onChange={(e) => setstreat(e.target.value)}
            required
          />
        </div>
        <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="firstName">
          Codigo postal
          </label>
          <input
            className="shadow appearance-none border rounded-none w-full py-2 px-3  leading-tight focus:outline-none focus:shadow-outline"
            id="cp"
            type="text"
            placeholder="Codigo postal"
            name="cp"
            onChange={(e) => setnumber(e.target.value)}
            required
          />
        </div>
        <div className="mb-6">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="lastName">
          Numero de casa
          </label>
          <input
            className="shadow appearance-none border rounded-none w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
            id="number_home"
            type="text"
            placeholder="Numero de casa"
            name="number_home"
            onChange={(e) => setcp(e.target.value)}
            required
          />
        </div>
     
        <div className="flex items-center justify-between">
          <button
            className="bg-white text-black font-bold py-1 px-3 rounded-none focus:outline-none focus:shadow-outline"
            type="submit"
            style={{fontSize : "12px"}}
          >
            Guardar
          </button>
     
          <a href="/"
            className="text-white font-bold py-1 px-3 rounded-none focus:outline-none focus:shadow-outline"
            style={{fontSize : "12px"}}>
            Regresar
          </a>
      
 
        </div>
       </form>
    </div>
  );
};

export default FormDir;
