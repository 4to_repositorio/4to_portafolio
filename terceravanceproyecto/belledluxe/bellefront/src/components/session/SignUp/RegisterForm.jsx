import React, { useState } from 'react';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuthStore } from '../../../store/auth';
import { register } from '../../../utils/auth';

const RegisterForm = () => {



  const [username, setUsername] = useState('');
  //const [email, setemail] = useState('');
  const [firstName, setfirstName] = useState('');
  const [lastName, setlastName] = useState('');
  const [lastName2, setlastName2] = useState('');
  const [phone, setphone] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');

  const isLoggedIn = useAuthStore((state) => state.isLoggedIn);
  const navigate = useNavigate();
  
  useEffect(() => {
    if (isLoggedIn()) {
        navigate('/');
    }
  }, []);

  const resetForm = () => {
    setUsername('');
    setemail('');
    setfirstName('');
    setlastName('');
    setlastName2('');
    setphone('');
    setPassword('');
    setPassword2('');

};
  const handleSubmit = async(e) => {
    e.preventDefault();
    const { error } = await register(username, firstName, lastName, lastName2, phone, password, password2);
    if (error) {
        alert(JSON.stringify(error));
    } else {
        navigate('/');
        resetForm();
    }
  };

  return (
    <div className="flex justify-center items-center h-screen">
      <form onSubmit={handleSubmit} className="border shadow-md px-8 pt-6 pb-8 mb-4">

        <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="username">
          Nombre de usuario
          </label>
          <input
            className="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="username"
            type="text"
            placeholder="Nombre de usuario"
            name="username"    
            maxLength={15}
            onChange={(e) => setUsername(e.target.value)}
            required
          />
        </div>

        <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="firstName">
          Nombre
          </label>
          <input
            className="shadow appearance-none border rounded-none w-full py-2 px-3  leading-tight focus:outline-none focus:shadow-outline"
            id="firstName"
            type="text"
            placeholder="Nombre"
            name="firstName"
            maxLength={35}
            onChange={(e) => setfirstName(e.target.value)}
            required
          />
        </div>
        <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="lastName">
          Primer apellido
          </label>
          <input
            className="shadow appearance-none border rounded-none w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
            id="lastName"
            type="text"
            placeholder="Primer apellido"
            name="lastName"
            maxLength={15}
            onChange={(e) => setlastName(e.target.value)}
            required
          />
        </div>
        <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="lastName2">
          Segundo apellido
          </label>
          <input
            className="shadow appearance-none border rounded-none w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
            id="lastName2"
            type="text"
            placeholder="Segundo apellido"
            name="lastName2"
            maxLength={15}
            onChange={(e) => setlastName2(e.target.value)}
          />
        </div>
        <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="phone">
          Número de teléfono
          </label>
          <input
            className="shadow appearance-none border rounded-none w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
            id="phone"
            type="number"
            placeholder="Número de teléfono"
            name="phone"
            maxLength={10}
            min={0}
            onChange={(e) => setphone(e.target.value)}
            required
          />
        </div>
        <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="password">
          Contraseña
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            id="password"
            type="password"
            placeholder="********"
            name="password"
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </div>
        <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="password2">
          Confirmar contraseña
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            id="password2"
            type="password"
            placeholder="********"
            name="password2"
            onChange={(e) => setPassword2(e.target.value)}
            required
          />
        </div>
        <div className="flex items-center justify-between">
          <button
            className="border text-white font-bold py-1 px-3 rounded focus:outline-none focus:shadow-outline"
            type="submit"
            style={{fontSize : "12px"}}
          >
            Crear
          </button>
          <a href="/login"
            className="text-white font-bold py-1 px-3 rounded-none focus:outline-none focus:shadow-outline"
            style={{fontSize : "12px"}}>
            Cambiar a iniciar sesion
          </a>
        </div>
      </form>
    </div>
  );
};

export default RegisterForm;
