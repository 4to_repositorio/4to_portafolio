import { useState } from 'react';
import { Typography } from "@material-tailwind/react";
import { FaFacebook, FaTiktok, FaInstagram } from 'react-icons/fa';
import { Input, Button } from "@material-tailwind/react";

export default function FooterTienda() {
  const [email, setEmail] = useState('');

  const handleSubscribe = async () => {
    try {
      const response = await fetch('https://tu-backend.com/send-email', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email: email }),
      });
      
      if (response.ok) {
        alert('Correo electrónico enviado con éxito');
        setEmail('');
      } else {
        alert('Hubo un problema al enviar el correo electrónico');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const handleInputChange = (e) => {
    setEmail(e.target.value);
  };

  return (
    <>
      <div>
        <footer style={{ border: '1px solid white', textAlign: 'center' }}>
          <div style={{ margin: '0 auto', padding: '20px', maxWidth: '800px' }}>
            <center>
              <form className="mt-8 mb-2 w-80 max-w-screen-lg sm:w-96">
                <Input
                  size="lg"
                  placeholder="name@mail.com"
                  className="border-white text-white !rounded-none"
                  labelProps={{
                    className: "before:content-none after:content-none",
                  }}
                  value={email}
                  onChange={handleInputChange}
                />
                <Button className="mt-6 bg-transparent border border-white text-white rounded-none" fullWidth onClick={handleSubscribe}>
                  Subscribirte
                </Button>
              </form>
            </center>
            <div style={{ display: 'flex', justifyContent: 'center', gap: '20px', marginTop: '20px' }}>
              <a href="https://www.facebook.com/belleedeluxe" style={{ color: 'white' }}>
                <FaFacebook />
              </a>
              <a href="https://www.tiktok.com/@belleedeluxe" style={{ color: 'white' }}>
                <FaTiktok />
              </a>
              <a href="https://www.instagram.com/belleedeluxe/" style={{ color: 'white' }}>
                <FaInstagram />
              </a>
            </div>

            <Typography style={{ marginTop: '20px', color: 'white' }}>
              &copy; 2024 All RUBIS COLLECTION | @belledeluxe. Tijuana, Baja California.
            </Typography>
            <Typography>
              <a href="/politicas" style={{ color: 'white', textDecoration: 'none' }}>Política de privacidad y cookies | Condiciones de venta</a>
            </Typography>
          </div>
        </footer>
      </div>
    </>
  );
}
