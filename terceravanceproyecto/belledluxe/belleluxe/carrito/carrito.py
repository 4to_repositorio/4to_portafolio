from django.db import transaction
from decimal import Decimal
from core.models import Product, ItemPedido, Pedido



class Carrito():

    def __init__(self, request):
        self.session = request.session 
        carrito = self.session.get('car')
        if 'car' not in request.session:
            carrito = self.session['car'] = {} 
        self.carrito = carrito

    def add(self, product, qty):

        product_id = str(product.id)

        if product_id in self.carrito:
            self.carrito[product_id]['qty'] = qty
        else:
            self.carrito[product_id] = {'price': str(product.price), 'qty': qty}
        self.save()


    def __iter__(self):

        product_ids = self.carrito.keys()
        products = Product.objects.filter(id__in=product_ids)
        carrito = self.carrito.copy()

        for product in products:
            carrito[str(product.id)]['product'] = product

        for item in carrito.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['qty']
            yield item


    def __len__(self):
        return sum(item['qty'] for item in self.carrito.values())
    
    def update(self, product, qty):
        product_id = str(product)
        if product_id in self.carrito:
            self.carrito[product_id]['qty'] = qty
        self.save()

    def get_total_price(self):
        return sum(Decimal(item['price']) * item['qty'] for item in self.carrito.values())
        
    def delete(self, product):
        product_id = str(product)

        if product_id in self.carrito:
            del self.carrito[product_id]
            print(product_id)
            self.save()

    def save(self):
        self.session.modified = True






