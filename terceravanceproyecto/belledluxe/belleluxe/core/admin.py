from django.contrib import admin

# Register your models here.

from .models import *

@admin.register(campaign)
class CampaignAdmin(admin.ModelAdmin):
    list_display=[
        "lasteditor", 
        "name"
    ]

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display=[
        "lasteditor", 
        "name"
    ]


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display=[
        "user", 
        "name_first", 
        "last_name_pat",
        "last_name_mat",
        "phone",
        "cp",
        "streat",
        "number_home"
    ]

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display=[
        "lasteditor",
        "name",
        "imagen1",
        "campaign",
        "gender",
        "category",
        "description",
        "size",
        "price",
        "status",
        "stock"
    ]

@admin.register(ItemPedido)
class ItemPedidoAdmin(admin.ModelAdmin):
    list_display=[
        "pedido",
        "product",
        "cantidad",
        'codigo_producto'
    ]

@admin.register(Pedido)
class PedidoAdmin(admin.ModelAdmin):
    list_display=[
        "usuario",
        "codigo_pedido"
    ]

@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    list_display=[
        "user",
        "product",
        'quantity'
    ]
