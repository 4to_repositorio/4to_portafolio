from django.shortcuts import render
from rest_framework import status, generics, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
from api.serializers import *
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.decorators import api_view, permission_classes
import json
from rest_framework.views import APIView
from core.models import *
from rest_framework import permissions
from .serializers import *
from django.core import serializers
from django.utils.crypto import get_random_string
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.permissions import IsAdminUser
# Create your views here.


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


@api_view(['POST'])
def RegistroAPI(request):
    if request.method == 'POST':
        data = request.data
        username = data.get('username')
        firstName = data.get('firstName')
        lastName = data.get('lastName')
        lastName2 = data.get('lastName2')
        phone = data.get('phone')
        password = data.get('password')
        password2 = data.get('password2')

        try:
            if password == password2:
                if User.objects.filter(username=username).exists():
                    return Response({'error': 'El usuario ya existe'}, status=400)
                else:
                    if len(password) < 6:
                        return Response({'error': 'La contraseña es corta'}, status=400)
                    else:
                        user = User.objects.create_user(username=username, password=password)
                        user_cus = Customer(user=user, name_first=firstName, last_name_pat=lastName, last_name_mat=lastName2, phone=phone, cp='', streat='', number_home='')
                        
                        user_cus.save()
                        return Response({'success': 'Se creó el usuario'}, status=200)
            else:
                return Response({'error': 'Las contraseñas no coinciden'}, status=400)
        except Exception as e:
            return Response({'error': f'Algo salió mal mientras registrabas tu cuenta: {str(e)}'}, status=400)
    else:
        return Response({'error': 'Método no permitido'}, status=405)

@api_view(['GET'])
def getRoutes(request):
    routes = [
        '/api/token/',
        '/api/register/',
        '/api/token/refresh/',
        '/api/test/'
    ]
    return Response(routes)

@api_view(['POST'])
def check_admin_status(request):
    if request.user.is_authenticated:
        is_admin = request.user.is_superuser
        return Response({'isAdmin': is_admin})
    else:
        return Response({'error': 'User is not authenticated'}, status=401)

class GetCustomerView(APIView):
    def get(self, request, format=None):
        try: 
            user = self.request.user

            user_obj = User.objects.get(id=user.id)
      

            customer_profile = Customer.objects.get(user=user.id)
    

            serializer = seriCustomer(customer_profile)

            return Response({'profile': serializer.data, 'username': str(user.username)})
        except User.DoesNotExist:
            return Response({'error': 'El usuario no existe.'})
        except Customer.DoesNotExist:
            return Response({'error': 'El perfil de cliente no existe.'})
        except Exception as e:
            return Response({'error': 'Algo salió mal al intentar recuperar el perfil. Detalle: {}'.format(str(e))})

class APIListaProductos(generics.ListAPIView):
    serializer_class = SeriProduct
    queryset = Product.objects.filter(status=True)

class APIProductos(generics.RetrieveAPIView):
    serializer_class = SeriProduct
    queryset = Product.objects.filter(status=True)


    

class AddToCartAPIView(APIView):
    def post(self, request):
        if request.user.is_authenticated:
            data = request.data
            quantity = data.get('quantity')
            product_id = data.get('product_id')
            try:
                product = Product.objects.get(id=product_id)
                carrito, created = Cart.objects.get_or_create(user=request.user, product=product_id)
                
                if not created:
                    carrito.quantity += quantity
                else:
                    carrito.quantity = quantity
                
                carrito.save()
                
                return Response({'success': 'Se agrego el producto'}, status=200)
                
            except Product.DoesNotExist:
                return Response({"error": "Product does not exist"}, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response({'error': 'User is not authenticated'}, status=status.HTTP_401_UNAUTHORIZED)


class APIcart_detail(APIView):
    def get(self, request):
        if request.user.is_authenticated:
            user = request.user
            try:
                carts = list(Cart.objects.filter(user=user).values_list('product', flat=True))
                productos = Product.objects.filter(id__in=carts)
                serializer = SeriProduct(productos, many=True)
                qty=sericard(Cart.objects.filter(user=user), many=True)
              
                return Response({"carrito": serializer.data, 'cantidad':qty.data})
            except Cart.DoesNotExist:
                return Response({"error": "tu carrito does not exist"}, status=404)

        else:
            return Response({'error': 'User is not authenticated'}, status=status.HTTP_401_UNAUTHORIZED)

class BorraraCarritoApi(APIView):
    def post(self, request):
        if request.user.is_authenticated:
            data = request.data
            try:
                dp = data.get('product_id')
                producto = Cart.objects.filter(product=dp)
                producto.delete()
            except Cart.DoesNotExist:
                return Response({"error": "tu carrito does not exist"}, status=404)
        else:
            return Response({'error': 'User is not authenticated'}, status=status.HTTP_401_UNAUTHORIZED)








class APIdireccion(APIView):
    def post(self, request):
        if request.user.is_authenticated:

            data = request.data
            streat  = data.get('streat')
            cp = data.get('cp')
            nc = data.get('number_home')
            try:
                cust = Customer.objects.get(user=request.user)
                cust.cp=cp
                cust.streat=streat
                cust.number_home=nc
                cust.save()
        
            except Customer.DoesNotExist:
                return Response({"error": "cliente does not exist"}, status=status.HTTP_404_NOT_FOUND)
            else:
                return Response({'error': 'User is not authenticated'}, status=401)
        else:
            return Response({'error': 'User is not authenticated'}, status=401)




















# Cruds
class CrudCampaign(viewsets.ModelViewSet):
    serializer_class = SeriCampaign
    queryset = campaign.objects.all()

class CrudCategory(viewsets.ModelViewSet):
    serializer_class = SeriCategory
    queryset = Category.objects.all()

class CrudCustomer(viewsets.ModelViewSet):
    # permission_classes = [IsAdminUser]
    serializer_class = seriCustomer
    queryset = Customer.objects.all()

class CrudProductos(viewsets.ModelViewSet):
    # permission_classes = [IsAdminUser]
    parser_classes = (MultiPartParser, FormParser)
    serializer_class = SeriProduct
    queryset = Product.objects.all()
        
class CrudItemPedido(viewsets.ModelViewSet):
    # permission_classes = [IsAdminUser]
    serializer_class = SeriItemPedido
    queryset = ItemPedido.objects.all()

class CrudPedido(viewsets.ModelViewSet):
    # permission_classes = [IsAdminUser]
    serializer_class = SeriPedido
    queryset = Pedido.objects.all()


class GetUsersView(APIView):
  #  permission_classes = [AllowAny]

    def get(self, request, format=None):
        users = User.objects.all()

        users = UserSerializer(users, many=True)
        return Response(users.data)




class pedidoAPI(APIView):
    def post(self, request):
        if request.user.is_authenticated:
            data = request.data
            try:
            
                nuevo_pedido = Pedido.objects.create(usuario=request.user, codigo_pedido=get_random_string(length=8))

                
                productos_carrito = data.get('productos', [])

                for producto in productos_carrito:
                    product_id = producto.get('id')
                    cantidad = producto.get('cantidad')

                    product = Product.objects.get(id=product_id)
                    codigo_producto = get_random_string(length=8)
                    ItemPedido.objects.create(pedido=nuevo_pedido, product=product, cantidad=cantidad, codigo_producto=codigo_producto)
                    product.stock -= cantidad
                    product.save()  
                carito = Cart.objects.filter(user=request.user)
                carito.delete()                   

                return Response({"mensaje": "Pedido realizado exitosamente"}, status=status.HTTP_201_CREATED)

            except Exception as e:
                return Response({'error': 'Algo salió mal al intentar recuperar el perfil. Detalle: {}'.format(str(e))})

        else:
            return Response({'error': 'El usuario no está autenticado'}, status=401)





class viewpedidoAPI(APIView):
    def get(self, request):
        if request.user.is_authenticated:
            
            try:
                pedido = Pedido.objects.filter(usuario=request.user) 
                listad=[]
                total=0
                for i in pedido:
                    productoo = {}
                    
                    item = ItemPedido.objects.filter(pedido=i.id) 
                    productoo['cadigo']= i.codigo_pedido
                    productoo['fecha']= i.fecha.strftime('%Y-%m-%d')
                    sublista=[]
                    for ii in item:
                        proditem = {}
                        nombre=ii.product.name
                        precio = ii.product.price
                        cantidad = ii.cantidad
                        productoTotal = cantidad * precio
                        total += productoTotal

                        proditem['itemnombre']=nombre
                        proditem['cantidad']=cantidad
                        proditem["totalproducto"]=productoTotal
                        sublista.append(proditem)
                    productoo['items']=sublista
                    productoo["total"]=total
                    listad.append(productoo)
                respuesta={}
                respuesta["pedidos"]=listad
                respuesta={"respuesta":respuesta }
                return Response(respuesta)
            except Exception as e:
                return Response({"error": str(e)}, status=500)
        else:
                return Response({'error': 'El usuario no está autenticado'}, status=401)




class APIListaProductosH(generics.ListAPIView):
    serializer_class = SeriProduct
    queryset = Product.objects.filter(status=True, gender='Hombre')

class APIListaProductosM(generics.ListAPIView):
    serializer_class = SeriProduct
    queryset = Product.objects.filter(status=True, gender='Mujer')