import { useState, useEffect } from 'react';
import '../App.css';

const url = "https://jsonplaceholder.typicode.com/todos"


export default function Todos() {
  const[data, SetData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(result => {
        setIsLoading(false);
        SetData(result);
      }, error => {
        setIsLoading(false);
        setError(error);
      })
  },[]);

  const getContent = () => {
    if (isLoading) {
      return (
        <div className="App">
          <h4>Loading Data ...</h4>
          <progress value={null} />
        </div>
      );
    }
  

    if (error) {
      return <h4>error</h4>
    }

  
    return (
        <div className="App">
          <div className='row'>
            <div className="col-2"></div>
            <div className="col-8">
              <table className='table table-striped'>
                <thead>
                    <br />
                    <tr>
                        <th> 7. LISTA DE TODOS LOS PENDIENTES SIN RESOLVER (IDs y USERIDs)</th>
                    </tr>
                  
                  <tr className="border-b border-blue-gray-100 bg-blue-gray-50 p-4">
                  
                    <th>ID</th>
                    <th>USER ID</th>
                   
                  </tr>
                </thead>
                <tbody>
                {data.filter(todo => !todo.completed).map(todo => (
                  <tr className="border-b border-blue-gray-100 bg-blue-gray-50 p-4" key={todo.id}>
                    <td>{todo.id}</td>
                    <td>{todo.userId}</td>
                    
                  </tr>
                ))}
                </tbody>
              </table>
            </div>
            <div className="col-2"></div>
          </div>
        </div>
      );
    }
  
  console.log(data)

  return (
    <div className="App">
      {getContent()}
    </div>
  );

}