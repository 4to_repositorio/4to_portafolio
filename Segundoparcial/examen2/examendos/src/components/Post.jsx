import { useState, useEffect } from 'react';
import '../App.css';

const url = "https://jsonplaceholder.typicode.com/todos"


export default function Todos() {
  const[data, SetData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(result => {
        setIsLoading(false);
        SetData(result);
      }, error => {
        setIsLoading(false);
        setError(error);
      })
  },[]);

  const getContent = () => {
    if (isLoading) {
      return (
        <div className="App">
          <h4>Loading Data ...</h4>
          <progress value={null} />
        </div>
      );
    }
  

    if (error) {
      return <h4>error</h4>
    }

  
    return (
        <div className="App">
          <div className='row'>
            <div className="col-2"></div>
            <div className="col-8">
            <table className="border-b border-blue-gray-100 bg-blue-gray-50 p-4">
                <thead>
                    <br />
                    <tr>
                        <th>LISTA DE TODOS LOS PENDIENTES </th>
                    </tr>
                  
                  <tr>
                  
                    <th className="border-b border-blue-gray-100 bg-blue-gray-50 p-4">POST ID</th>
                    <th className="border-b border-blue-gray-100 bg-blue-gray-50 p-4">USER ID</th>
                    <th className="border-b border-blue-gray-100 bg-blue-gray-50 p-4">POST TITLE</th>
                    <th className="border-b border-blue-gray-100 bg-blue-gray-50 p-4">COMPLETED</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map(todo => (
                    <tr className="border-b border-blue-gray-100 bg-blue-gray-50 p-4" key={todo.id}>
                      <td >{todo.id}</td>
                      <td>{todo.userId}</td>
                      <td>{todo.title}</td>
                      <td>{todo.completed ? 'Yes' : 'No'}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            <div className="col-2"></div>
          </div>
        </div>
      );
    }
  

  console.log(data)

  return (
    <div className="App">
      {getContent()}
    </div>
  );

}