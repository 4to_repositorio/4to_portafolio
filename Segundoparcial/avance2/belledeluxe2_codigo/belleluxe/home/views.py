from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from core.models import Customer, Category
from django.contrib.auth.mixins import LoginRequiredMixin

from django.urls import reverse_lazy, reverse
from .forms import *

# Inicio ---------------------------------------------------------
class inicio(generic.View):
    tamplate_name="home/inicio.html"
    context={} 
    
    def get(self, request, *args, **kwargs):
        self.context= {
           "category": Category.objects.all()
       }
        return render(request, self.tamplate_name, self.context)

# Info ---------------------------------------------------------
class nosotros(generic.View):
    tamplate_name="home/info/nosotros.html"
    context={}
    def get(self, request):
        return render(request, self.tamplate_name, self.context)
    
class blog(generic.View):
    tamplate_name="home/info/blog.html"
    context={}
    def get(self, request):
        return render(request, self.tamplate_name, self.context)
    
class servicios( generic.View):
    tamplate_name="home/info/servicios.html"
    context={}
    def get(self, request):
        return render(request, self.tamplate_name, self.context)

class politicas(generic.View):
    tamplate_name="home/info/politicas.html"
    context={}
    def get(self, request):
        return render(request, self.tamplate_name, self.context)


    
    
# Sistema de acceso ---------------------------------------------------------

# resgistro 
class Acceso(generic.View):
    template_name = "home/acceso/iniciosesion.html"
    context = {}
    user = ""
    password = ""
    form_class = inseForm
    
    def get(self, request):
        self.form_class = inseForm()
        self.context = {
            "form": self.form_class
        }
        return render(request, self.template_name, self.context)
    
    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("/")
        else:
            return redirect("/acceso/")

# cerrar sesion
def cierre(request):
    logout(request)
    return redirect('/')


# Registrarse con formulario anidado
"""class registrar(generic.CreateView):
    model = User
    fields = ["username", "password1", "password2"]"""

        


class registrar(generic.CreateView):
    template_name = "home/acceso/registro.html"
    form_class = RegForm
    success_url = reverse_lazy("home:inicio")

    def form_valid(self, form):
        response = super().form_valid(form)
        user = form.save()
        if user is not None:

            username = form.cleaned_data.get("username")
            password1 = form.cleaned_data.get("password1")
            cliente = authenticate(self.request, username=username, password=password1)
        
            if cliente is not None:
                login(self.request, cliente)
                cus = Customer(user=user, name_first = form.cleaned_data['name_first'], last_name_pat = form.cleaned_data['last_name_pat'], last_name_mat = form.cleaned_data['last_name_mat'], phone = form.cleaned_data['phone'], cp = form.cleaned_data['cp'], streat = form.cleaned_data['streat'], number_home = form.cleaned_data['number_home'])
                cus.save()        
            
        return response
    
# actualizar datos

class UpdateProfile(LoginRequiredMixin, generic.UpdateView):
    template_name = "home/acceso/editarperfil.html"
    model = Customer
    form_class = ActualizarUs
    success_url = reverse_lazy("home:perfil")
    login_url = reverse_lazy("home:inicio")

    def get_success_url(self):
        pk = self.kwargs["pk"]
        return reverse("home:perfil", kwargs={"pk":pk})

class perfil(LoginRequiredMixin, generic.View):
    template_name="home/acceso/perfil.html"
    context={}
    model=Customer
    login_url = reverse_lazy("home:inicio")
    
    def get(self, request, pk):
        self.context = {
            "Customer" : User.objects.get(id=pk)
        }
        return render(request, self.template_name, self.context)
   
