export default function Text(props){
    return (
        <p style= {styles.colorText}>{props.p} </p>
    )
}

const styles = {
    colorText: {
      color:"green", 
      textAlign: "center",
      fontSize: 32, 
      fontFaimily: 'Roboto, Helvetica', 
    },
  }
  