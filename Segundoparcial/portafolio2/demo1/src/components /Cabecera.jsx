import logo from '../logo.svg';


//Esta es la cabecera, la bienvenida , en esta la podemos caracterizar como que ultiza el logo de la compañia 
//es una funcion que puede ser utilizada en cualquier lado. 
export default function Cabecera(){
    return(
        <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
              hola mundo
        </p>
          Learn React
      </header>
    );
}