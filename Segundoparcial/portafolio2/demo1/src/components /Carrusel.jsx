import React from 'react';
import Slider from 'react-slick';

//un Carrusel de foto, aunque no tiene las fotos pero es la estructura de el. 
//tiene algunas caracteristicas de react-slick, para que tenga su funcionamiento. 
export default function Carrusel (props) {
  return (
    <Slider {...settings}>
      <div>
        <h3>Slide 1</h3>
      </div>
      <div>
        <h3>Slide 2</h3>
      </div>
      <div>
        <h3>Slide 3</h3>
      </div>
      <div>
        <h3>Slide 4</h3>
      </div>
      <div>
        <h3>Slide 5</h3>
      </div>
    </Slider>
  );

} ; 
//caracteristicas, funciones y demas. 
const Carrusel = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
  };
};


