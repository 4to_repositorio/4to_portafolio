export default function Table3x3(props) {
    //Esta tabala que puede ser el orden de la ifnormacion dentro de una pagina
    //delimita el orden, las columnas y las filas, y la ifnormacion que se mantenga. 
    return(
        <table>
            <thead>
                <th>{props.th1} </th>
                <th>{props.th2} </th>
                <th>{props.th3} </th>
            </thead>
            <tbody>
                <tr>
                <th>Cell 1 </th>
                <th>Cell 2 </th>
                <th>Cell 3 </th> 
                </tr>
                <tr>
                <th>Cell 4 </th>
                <th>Cell 5 </th>
                <th>Cell 6 </th> 
                </tr>
                <tr>
                <th>Cell 7 </th>
                <th>Cell 8 </th>
                <th>Cell 9 </th> 
                </tr>
            </tbody>
        </table>
    )
}