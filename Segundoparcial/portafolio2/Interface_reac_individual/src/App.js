
import './App.css';
import Cabesera from './componetes/cabecera';
import Carrusel from './componetes/carrusel';
import Tirabaja from './componetes/Tirabaja.jsx';

const cssbootstrap = require('./static/css/bootstrap.min.css')

export default function App() {
  return (
    <div className="App">
      <a href={cssbootstrap} rel="stylesheet" integrity="" crossorigin=""/>
      <script src={jsbootstrap} integrity="" crossorigin=""></script>
      <Cabecera/>
      
      <Carrusel/>
      <br/>
      <br/>
      <br/>

      <Tirabaja/>
    </div>
  );
}


