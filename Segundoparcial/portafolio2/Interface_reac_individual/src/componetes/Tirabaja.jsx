export default function Tirabaja(){
    return(
        <footer class="text-center footer-style">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 footer-col">
                        <h3>Redes</h3>
                            <p>
                                Sitio web <br />
                                Correo Electronico <br />
                                Tiktok
                            </p>
                    </div>
                    <div class="col-md-4 footer-col">
                        <h3>Temas</h3>
                        <ul class="list-inline">
                            <li>
                                <h6>Eleccion 1</h6> 
                            </li>
                            <li>
                                <h6>Eleccion 2</h6> 
                            </li>
                            <li>
                                <h6>Eleccion 3</h6> 
                            </li>
                          
                        </ul>
                    </div>
                    <div class="col-md-4 footer-col">
                        <h3>Contactos</h3>
                        <p>Haz click</p>
                    </div>
                </div>
            </div>
        </footer>
            )
}