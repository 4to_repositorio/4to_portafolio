const imgC1=require('./recursos/carrusel/mari1.png')
const imgC2=require('./recursos/carrusel/mari2.png')
const imgC3=require('./recursos/carrusel/mari3.png')
export default function Carrusel(){
    return(
        <div id="carouselExampleIndicators" class="carousel slide">
            
            <div class="carousel-inner">
            <div class="container-md">
                <div class="carousel-item active">
                <img src={imgC2} class="d-block w-100" alt="..."/>
                </div>
                <div class="carousel-item">
                <img src={imgC1} class="d-block w-100" alt="..."/>
                </div>
                <div class="carousel-item">
                <img src={imgC3} class="d-block w-100" alt="..."/>
                </div>
                </div>
            </div>
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                <span class="carousel-control-prev-icon"  style={{backgroundColor: '#f7431c' }}  aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>

            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
            <span className="carousel-control-next-icon" style={{backgroundColor: '#f7431c' }} aria-hidden="true"></span>  
            <span class="visually-hidden">Next</span>
            </button>
            </div>
    );
}