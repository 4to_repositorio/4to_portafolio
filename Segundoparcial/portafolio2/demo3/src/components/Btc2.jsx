import { useState, useEffect } from 'react';
import '../App.css';

const url = "https://api.coindesk.com/v1/bpi/currentprice.json"
//asi como podemos poner las funciones de cosas como de diseño, es posible realizar consultas a bases de datos 
//para obtener informacion atraves de apis esta es la manera correcta de hacerlo en reac. 

export default function Btc2() {
  const[data, SetData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  //estas lineas son utilizadas para almacenar informacion, data puede guardar cualquier tipo de dato. 
  //para actualizar la informacion, para cargarla. 

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(result => {
        setIsLoading(false);
        SetData(result);
      }, error => {
        setIsLoading(false);
        setError(error);
      })
  },[]);

  const getContent = () => {
    if (isLoading) {
      return (
        <div className="App">
          <h4>Loading Data ...</h4>
          <progress value={null} />
        </div>
      );
    }
  
    //en caso de que haya algun error este se encargara de mostrartelo para realizar las 
    //acciones correspondientes. 
    if (error) {
      return <h4>error</h4>
    }

  

    return (
      <div className="App">
        <h1>BTC to USD |EUR |GBP</h1>
        <h3>BTC to USD</h3>


        <div class = "col-2">

        </div>


        <div class = "col-8">
          <table class = 'table table-striped'>
            <thead>
              <th>RATE</th>
              <th>RATE FLOAT</th>
              <th>DESCRIPTION</th>
              <th>UPDATE</th>
            </thead>
            <tbody>
              <tr>
                <td>{data["bpi"]["USD"].rate}</td>
                <td>{data["bpi"]["USD"].rate_float}</td>
                <td>{data["bpi"]["USD"].description}</td>
                <td>{data["time"].updated}</td>
              </tr>
              <tr>
                <td>{data["bpi"]["EUR"].rate}</td>
                <td>{data["bpi"]["EUR"].rate_float}</td>
                <td>{data["bpi"]["EUR"].description}</td>
                <td>{data["time"].updated}</td>
              </tr>
              <tr>
                <td>{data["bpi"]["GBP"].rate}</td>
                <td>{data["bpi"]["GBP"].rate_float}</td>
                <td>{data["bpi"]["GBP"].description}</td>
                <td>{data["time"].updated}</td>
              </tr>
            </tbody>
          </table>
        </div>


        <div class= "col-2">

        </div>
        
      </div>
    );
  }

  console.log(data)

  return (
    <div className="App">
      {getContent()}
    </div>
  );

}