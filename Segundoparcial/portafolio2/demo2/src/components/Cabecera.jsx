import logo from '../logo.svg';
//Bueno esta es la cabecera princial es decir es la bienvenida y la primera expresion que se tienen los clientes
export default function Cabecera(){ 
  //estas funciones pueden ser utilizados en donde queramos siempre y cuando esten exportados en la aplicacion principal 
    return(
        <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
              hola mundo
        </p>
          Learn React
      </header>
    );
}