//esta es la funcion de una tabla, esta nos puede ayudar a tener la informacion o lo que queramos de manera ordenada 

export default function Table3x3(props) {
    //siempre exporando la funcion 
    return(
        <table>
            <thead>
                <th>{props.th1} </th>
                <th>{props.th2} </th>
                <th>{props.th3} </th>
            </thead>
            <tbody>
                <tr>
                <th>Cell 1 </th>
                <th>Cell 2 </th>
                <th>Cell 3 </th> 
                </tr>
                <tr>
                <th>Cell 4 </th>
                <th>Cell 5 </th>
                <th>Cell 6 </th> 
                </tr>
                <tr>
                <th>Cell 7 </th>
                <th>Cell 8 </th>
                <th>Cell 9 </th> 
                </tr>
            </tbody>
        </table>
    )
}