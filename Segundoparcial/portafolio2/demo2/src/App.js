import './App.css'; 
import 'bootstrap/dist/css/bootstrap.min.css';


// import Cabecera from './components/Cabecera';

//estas son las funciones que se utlizan de los componentes, debemos de importarlas a esta aplciacion 
//para que pueda ser utilizado, debemos de tener cuidado con los espacion en las importaciones. 
import Welcome from './components/Welcome';
import Text from './components/Text';
import Images from './components/Imagess';
import Table3x3 from './components/Table3x3';
import Header from './components/Header';

 export default function App() {
  return (
      //como vemos aqui estamos utlizando el header, el texto, las imagenes y una tabla que nuestro profesor nos proporciono 
      //solo debemos de poner el nombre de las funciòn y listo. 
    <div className="App">
       <Header/>
     <Text p = "BELLE D' LUXE" />
     <h1> Hola Mundo 2</h1>
     <Images/>
     <Table3x3  th1={1} th2={2} th3={3}/>

     </div>
  );
}